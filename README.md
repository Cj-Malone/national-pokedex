# National Pokédex According to Pokémon Emerald

This is a set of Pokémon sprites, extracted from Pokémon Emerald by hand with [mGBA](https://github.com/mgba-emu/mgba) and this [patch](https://github.com/mgba-emu/mgba/issues/841).

